"""
Implement Students room using OOP:
Copy
Steve = Student("Steven Schultz", 23, "English")
Johnny = Student("Jonathan Rosenberg", 24, "Biology")
Penny = Student("Penelope Meramveliotakis", 21, "Physics")
print(Steve)
<name: Steven Schultz, age: 23, major: English>
print(Johnny)
<name: Jonathan Rosenberg, age: 24, major: Biology>
"""
class Student():

    def __init__(self, full_name, age, major):
        self.full_name = full_name
        self.age = age
        self.major = major

    def __str__(self):
        message = 'name: {}, age: {}, major: {}'.format(self.full_name, self.age, self.major)
        return message.title()



Steve = Student("Steven Schultz", 23, "English")
Johnny = Student("Jonathan Rosenberg", 24, "Biology")
Penny = Student("Penelope Meramveliotakis", 21, "Physics")

print(Steve)
print(Johnny)
print(Penny)
